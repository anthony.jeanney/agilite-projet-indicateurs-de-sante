Feature: Register
  Background: Delete test user
    Given I navigate to the main page of the application
    Then I navigate to the logout page of the application
    And I delete the test user from the database
    And I navigate to the register page of the application
    And I wait for the register page to load
    And I fill the register form with valid data
    And I submit the form
    And I wait for the main page to load
    Then I navigate to the login page of the application
    And I wait for the login page to load
    And I enter valid test credentials
    And I submit the form
    And I wait for the main page to load
    Then I should be logged in
    Then I navigate to the data/add page of the application
    And I wait for the data/add page to load

  Scenario: Check data
    Given I fill the data form
    And I submit the form
    Then I navigate to the data/visualise page of the application
    And I wait for the data/visualise page to load
#    Then I should see 2000-01-01 with 70 in weight data