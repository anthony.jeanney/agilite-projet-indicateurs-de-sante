Feature: Register
  Background: Delete test user
    Given I navigate to the main page of the application
    Then I navigate to the logout page of the application
    And I delete the test user from the database
    Then I navigate to the register page of the application

  Scenario: Register with valid data
    Given I wait for the register page to load
    Then I fill the register form with valid data
    And I submit the form
    And I wait for the main page to load
    Then I find "Bienvenue sur l'indicateur de santé Decathlon"