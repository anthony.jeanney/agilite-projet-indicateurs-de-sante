from selenium import webdriver
from selenium.webdriver.firefox.options import Options


def before_all(context):
    opts = Options()
    opts.headless = True
    context.browser = webdriver.Firefox(options=opts)
    context.browser.set_page_load_timeout(10)
    context.browser.implicitly_wait(10)
    context.browser.maximize_window()


def after_all(context):
    context.browser.quit()
