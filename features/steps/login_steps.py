from behave import when, then, step
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


@step('I enter valid login credentials')
def step_impl(context):
    context.browser.find_element(By.NAME, "username").send_keys("DoeJohn")
    context.browser.find_element(By.NAME, "password").send_keys("password")


@step("I wait for the main page to load")
def step_impl(context):
    WebDriverWait(context.browser, 10000).until(
        lambda x: x.current_url == "http://0.0.0.0:8000/"
    )


@step("I wait for the {page} page to load")
def step_impl(context, page):
    WebDriverWait(context.browser, 10000).until(
        lambda x: x.current_url == "http://0.0.0.0:8000/" + page
    )


@then("I should be logged in")
def step_impl(context):
    assert len(context.browser.find_elements(By.CLASS_NAME, 'btn-primary')) > 1
