from behave import given, step, when, then
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from health_indicator import HealthIndicator
from time import sleep
import os


@then('I navigate to the {page} page of the application')
def step_impl(context, page):
    context.browser.get("http://0.0.0.0:8000/" + page)


@then('I fill the register form with valid data')
def step_impl(context):
    context.browser.find_element(By.NAME, "username").send_keys("test")
    context.browser.find_element(By.NAME, "password").send_keys("test")
    context.browser.find_element(By.NAME, "firstname").send_keys("test")
    context.browser.find_element(By.NAME, "lastname").send_keys("test")
    context.browser.find_element(By.NAME, "birthdate").send_keys("2000-01-01")


@step('I submit the form')
def step_impl(context):
    context.browser.find_element(By.TAG_NAME, "form").submit()


@step('I delete the test user from the database')
def step_impl(_):
    database = HealthIndicator(os.path.dirname(os.path.abspath(__file__)) + "/../../src/database.key")
    database.get_database().remove_user("test")
    sleep(3)

@step('I enter valid test credentials')
def step_impl(context):
    context.browser.find_element(By.NAME, "username").send_keys("test")
    context.browser.find_element(By.NAME, "password").send_keys("test")
