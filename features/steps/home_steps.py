from behave import given, then
from selenium.webdriver.common.by import By


@given('I navigate to the main page of the application')
def step_impl(context):
    context.browser.get("http://0.0.0.0:8000/")
    assert "HealthIndicator" in context.browser.title


@then('I find "{title}"')
def step_impl(context, title):
    page_title = context.browser.find_element(By.TAG_NAME, "h1").text
    assert page_title == title
