from behave import given, then
from selenium.webdriver.common.by import By


@given("I fill the data form")
def step_impl(context):
    context.browser.find_element(By.NAME, "date").send_keys("2000-01-01")
    context.browser.find_element(By.NAME, "weight").send_keys("70")
    context.browser.find_element(By.NAME, "height").send_keys("250")
    context.browser.find_element(By.NAME, "sleep").send_keys("90")
    context.browser.find_element(By.NAME, "steps").send_keys("1000000")


@then("I should see {date} with {number} in {entry} data")
def step_impl(context, date, number, entry):
    main_element = context.browser.find_element(By.CSS_SELECTOR, ".overflow-y-scroll")
    row_entry = None
    row_val = None
    # test = None
    for div in main_element.find_elements(By.TAG_NAME, "div"):
        if div.find_element(By.TAG_NAME, "h2").text == entry:
            # test = div.find_element(By.TAG_NAME, "h2").text
            table = div.find_element(By.TAG_NAME, "table")
            tbody = table.find_element(By.TAG_NAME, "tbody")
            trs = tbody.find_elements(By.TAG_NAME, "tr")
            for tr in trs:
                tds = tr.find_elements(By.TAG_NAME, "td")
                row_entry = tds[0].text
                row_val = tds[1].text
    assert int(row_entry) == date
    assert int(row_val) == number
    # assert test == entry
