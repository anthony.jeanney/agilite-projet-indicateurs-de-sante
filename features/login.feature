Feature: Login
  Background:
    Given I navigate to the main page of the application
    Then I navigate to the logout page of the application
    Then I navigate to the login page of the application
    And I wait for the login page to load

  Scenario: Login with valid credentials
    When I enter valid login credentials
    And I submit the form
    And I wait for the main page to load
    Then I should be logged in