import pickle
from cryptography.fernet import Fernet

key = Fernet.generate_key()

pickle.dump(key, open("database.key", "wb"))
