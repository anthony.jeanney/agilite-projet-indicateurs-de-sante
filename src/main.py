"""
Main class
"""
import os.path
from datetime import datetime

from health_indicator import HealthIndicator


def main():
    """Exemple d'utilisation"""
    app = HealthIndicator(os.path.dirname(os.path.abspath(__file__)) + "/database.key")

    app.get_database().remove_user("DoeJohn")

    app.sign_up({
        "username": "DoeJohn",
        "password": "password",
        "firstname": "John",
        "lastname": "Doe",
        "gender": "M",
        "birthdate": str(datetime(2000, 1, 1))
    })
    _, user = app.login("DoeJohn", "password")

    user["data"].add_data(datetime(2020, 1, 1),
                          {"weight" : 70, "height" : 180, "steps" : 10000,
                           "sleep" : 8, "heartrate" : 60})
    user["data"].add_data(datetime(2020, 1, 2),
                          weight=80, height=180, steps=10200, sleep=7, heartrate=70)

    app.save_user(user)

    app.login("DoeJohn", "password")

    print(user["data"].get_all_latest_datas())


if __name__ == "__main__":
    main()
