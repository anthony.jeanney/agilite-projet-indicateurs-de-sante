"""
Module which contains the Main application class
"""
import pickle

from health_indicator.database import Database
from health_indicator.user import User


class HealthIndicator:
    """
    Class which represents the main application
    """

    def __init__(self, key):
        self._database = Database(
            "admin",
            "cJ4WAVjxKRtbRD82",
            "cluster0.8jfma.mongodb.net",
            "health_indicator",
            pickle.load(open(key, "rb"))
        )

    def __str__(self):
        return self.__class__.__name__

    def login(self, username, password):
        """
        Method which logs in a user
        :param username:
        :param password:
        :return the error if there is one and the user if there is one
        """
        user = self._database.get_user(username)
        if user is not None:
            if user["password"] == password:
                return None, user
            else:
                return 'Bad password', None
        return 'User does not exist', None

    def sign_up(self, form):
        """
        Method which signs up a user
        :param form:
        :param birthdate:
        :param gender:
        :param lastname:
        :param firstname:
        :param password:
        :param username:
        :return boolean:
        """
        if self._database.user_exists(form['username']):
            return False
        user = User(form)
        self._database.add_user(user)

        self.login(form['username'], form['password'])

        return True

    def save_user(self, user):
        """
        Method which saves the user
        :return:
        """
        self._database.update_user(user["_id"], user)

    def get_database(self):
        """
        Method which returns the database
        :return Database:
        """
        return self._database
