"""
Module which contains the Database class.
"""
from pymongo import MongoClient
from cryptography.fernet import Fernet

from health_indicator.user import User, UserData


class Database:
	"""
	Database class in order to represent the database.
	"""

	def __init__(self, username, password, cluster, database, key):
		self.client = MongoClient(
			f"mongodb+srv://{username}:{password}@{cluster}/{database}?retryWrites=true&w=majority",
		)
		self.collection = self.client["health_indicator"]["users"]
		self._key = key
		self._f = Fernet(self._key)

	def __str__(self):
		return str(self.client.server_info())

	def get_status(self):
		"""
		Method which returns the status of the database.
		:return:
		"""
		try:
			self.client.server_info()
			return True
		except MongoClient.errors.ServerSelectionTimeoutError:
			return False

	def close(self):
		"""
		Method which closes the connection with the database.
		:return:
		"""
		self.client.close()

	def add_user(self, user):
		"""
		Method which adds a user to the database.
		:param user:
		:return:
		"""
		if self.get_user(user["_id"]) is None:
			user = self.encrypt_user(user)
			self.collection.insert_one(user)

	def remove_user(self, username):
		"""
		Method which removes a user from the database.
		:param username:
		:return:
		"""
		self.collection.delete_one({"_id": username})

	def get_user(self, username):
		"""
		Method which returns a user.
		:param username:
		:return User:
		"""
		db_user = self.collection.find_one({"_id": username})
		if db_user is None:
			return None
		user = User({
			"username": db_user["_id"],
			"password": db_user["password"],
			"firstname": db_user["firstname"],
			"lastname": db_user["lastname"],
			"gender": db_user["gender"],
			"birthdate": db_user["birthdate"]
		})
		user["data"] = UserData(db_user["data"])
		return self.decrypt_user(user)

	def update_user(self, username, user):
		"""
		Method which updates a user.
		:param username:
		:param user:
		:return:
		"""
		user = self.encrypt_user(user)
		self.collection.replace_one({"_id": username}, user)

	def user_exists(self, username):
		"""
		Method which checks if a user exists
		:param username:
		:return bool:
		"""
		return self.get_user(username) is not None

	def encrypt_user(self, user):
		"""
		Method which encrypts data.
		:param user:
		:return User:
		"""
		return User(user).encrypt(self._f)

	def decrypt_user(self, user):
		"""
		Method which decrypts data.
		:param user:
		:return User:
		"""
		return User(user).decrypt(self._f)
