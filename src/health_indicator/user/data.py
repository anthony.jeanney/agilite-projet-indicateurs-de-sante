"""
Module wich contains the UserData class.
"""


class UserData(dict):
    """
    Class which contains the updatable data of a user
    """

    def __init__(self, data=None):
        super().__init__()
        if data is not None:
            for key in data:
                self[key] = data[key]
        else:
            self["weight"] = {}
            self["height"] = {}
            self["steps"] = {}
            self["sleep"] = {}
            self["heartrate"] = {}

    def add_data(self, date, form):
        """
        Method wich adds new datas to the user
        :param date: date of the data
        :param form: form with the data
        :return:
        """
        date = str(date)
        if 'weight' in form and form['weight'] != '':
            self["weight"][date] = str(form['weight'])
        if 'height' in form and form['height'] != '':
            self["height"][date] = str(form['height'])
        if 'steps' in form and form['steps'] != '':
            self["steps"][date] = str(form['steps'])
        if 'sleep' in form and form['sleep'] != '':
            self["sleep"][date] = str(form['sleep'])
        if 'heartrate' in form and form['heartrate'] != '':
            self["heartrate"][date] = str(form['heartrate'])

    def remove_data_entry(self, key, date):
        """
        Method wich removes the data of the key at the given date
        :param key: key of the data
        :param date: date of the data
        :return:
        """
        del self[key][str(date)]

    def get_all_latest_datas(self):
        """
        Method wich returns the latest data of the user.
        :return dict:
        """
        latest_datas = {}
        for key in self:
            latest_datas[key] = self.get_latest_data(key)

        return latest_datas

    def get_latest_data(self, key):
        """
        Method wich returns the latest value of the key
        :param key:
        :return value:
        """
        if len(self[key]) == 0:
            return None
        return self[key][max(self[key].keys())]

    def encrypt(self, crypting_key):
        """
        Method wich encrypts UserData
        :return UserData:
        """
        encrypted_data = {}
        for key, item in self.items():
            encrypted_data[key] = {}
            for date, value in item.items():
                encrypted_data[key][date] = crypting_key.encrypt(value.encode('utf-8'))
        return encrypted_data

    def decrypt(self, crypting_key):
        """
        Method wich decrypts UserData
        :return UserData:
        """
        for key, item in self.items():
            self[key] = {}
            for date, value in item.items():
                self[key][date] = crypting_key.decrypt(value).decode()

        return self
