"""
Module which contains the User class.
"""

from health_indicator.user import UserData


class User(dict):
    """
    Class which represents the user.
    """
    def __init__(self, form):
        """Creates a user from a form or a user."""
        super().__init__()
        if '_id' in form:
            self['_id'] = form['_id']
        else:
            self["_id"] = form['username']
        self["password"] = form['password']
        self["firstname"] = form['firstname']
        self["lastname"] = form['lastname']
        self["birthdate"] = form['birthdate']
        self["gender"] = form['gender']
        if 'data' in form:
            self["data"] = UserData(form['data'])
        else:
            self["data"] = UserData()

    def __str__(self):
        return self["_id"]

    def encrypt(self, crypting_key):
        """
        Method which encrypts User
        :return User:
        """
        encrypted_user = {}
        for key, item in self.items():
            if key not in ["_id", "data"]:
                encrypted_user[key] = crypting_key.encrypt(item.encode('utf-8'))
            elif key == "data":
                encrypted_user[key] = item.encrypt(crypting_key)
            else:
                encrypted_user[key] = item

        return encrypted_user

    def decrypt(self, crypting_key):
        """
        Method which decrypts User
        :return User:
        """
        for key, item in self.items():
            if key not in ["_id", "data"]:
                self[key] = crypting_key.decrypt(item).decode('utf-8')
            elif key == "data":
                self[key] = item.decrypt(crypting_key)
            else:
                self[key] = item

        return self
