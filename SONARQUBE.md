## Pour tester en local avec pylint et sonarqube:
- pytest -v -o junit_family=xunit1 --cov=src --cov-report xml:tests/coverage.xml --junitxml=tests/nosetests.xml

- pylint --exit-zero --disable=C0303,R0902,W0311 --output pylint-report.txt src

- --define sonar.python.xunit.reportPath=tests/nosetests.xml sonar.python.coverage.reportPaths=tests/coverage.xml sonar.python.pylint.reportPath=pylint-report.txt

