# Agilité: Gérer des indicateurs de santé
### Équipe: 418

- Leprêtre Hugo
- Jeanney Anthony
- Dumont Quentin
- Leroy Laury


# Features du projet :

Inscription et connexion d'un utilisateur, de manière sécurisée.
Ajout de données de santé et visualisation de l'historique. Possibilité de se déconnecter de supprimer son compte
(et ses données liés).

&rarr; Toutes les données en dehors du pseudonyme sont chiffrées.

Analyse du projet à chaque merge request et push sur les branches dév/master
via sonarqube sur l'adresse suivante: https://sonarcloud.io/project/overview?id=anthony.jeanney_agilite-projet-indicateurs-de-sante

Sonarqube analyse le coverage du code ainsi que les potentiels soucis de sécurité.

Déploiement automatique sur un serveur heroku à chaque push sur la branche dév (https://health-indicator-dev.herokuapp.com/logout)
et la branche master (https://health-indicator-prod.herokuapp.com/)

Test e2e via selenium+behave sur la CI, test unitaire via pytest sur la CI.

Déploiement automatique de la documentation sur gitlab (https://anthony.jeanney.gitlab.io/agilite-projet-indicateurs-de-sante/)
via Sphinx.

# Définition du projet:
étiquette Identified : Element identifié qui est dépendant d'un élément précédent pour être réalisé.

étiquette Ready: Element identifié qui est prêt à être réalisé.

étiquette In progress: Element identifié qui est en cours de réalisation.

étiquette Done: Element identifié qui est réalisé, et qui passe le check qualité de sonarqube. (Pour les éléments qui ne sont pas du code analysable
, par exemple la mise en place du CI, l'étiquette done est attribué lorsque l'élément est fonctionnel sur tout le groupe.)
