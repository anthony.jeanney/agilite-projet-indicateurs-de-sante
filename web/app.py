"""Flask app"""

from health_indicator import HealthIndicator
from health_indicator.user.data import UserData
import uuid
import os
from flask import Flask, render_template, request, redirect, url_for, session

database = HealthIndicator(os.path.dirname(os.path.abspath(__file__)) + "/../src/database.key")

app = Flask(__name__, static_folder='./static', template_folder='./templates')
app.config['SECRET_KEY'] = str(uuid.uuid4())


@app.route('/register', methods=['GET', 'POST'])
def signup():
	"""Render the registering page."""
	if 'user' in session:
		return redirect(url_for('index'))
	if request.method == 'POST':
		form = {}
		for (key, value) in request.form.items():
			form[key] = value
		if database.sign_up(form):
			return redirect(url_for('index'))
	return render_template(
		'register.html',
		user={'is_logged_in': False},
		title='Sign In'
	)


@app.route('/data/add', methods=['GET', 'POST'])
def adddata():
	"""Render the adddata page."""
	if 'user' not in session:
		return redirect(url_for('index'))

	user = {'is_logged_in': True, 'name': session['user']['_id']}
	if request.method == 'POST':
		form = {}
		date = None
		for (key, value) in request.form.items():
			if key == 'date':
				date = value
			else:
				form[key] = value
		if date is not None:
			updated_user = UserData(session['user']['data'])
			updated_user.add_data(date, form)
			database.save_user(session['user'])
			session['user'] = database.get_database().get_user(session['user']['_id'])
			return redirect(url_for('adddata'))
	return render_template(
		'data_form.html',
		user=user,
		title='Add Data'
	)


@app.route('/login', methods=['GET', 'POST'])
def login():
	"""Render the login page."""
	if 'user' in session:
		return redirect(url_for('index'))

	if request.method == 'POST':
		form = {}
		for (key, value) in request.form.items():
			form[key] = value
		username = form['username']
		password = form['password']
		error, user = database.login(username, password)
		if user is not None:
			session['user'] = user
			return redirect(url_for('index'))

		return render_template(
			'login.html',
			user={'is_logged_in': False, 'error': error},
			title='Login'
		)

	return render_template(
		'login.html',
		user={'is_logged_in': False, 'error': None},
		title='Login'
	)


@app.route('/delete', methods=['GET', 'POST'])
def delete():
	"""Render the login page."""
	if 'user' not in session:
		return redirect(url_for('index'))

	if request.method == 'POST':
		form = {}

		for (key, value) in request.form.items():
			form[key] = value

		username = form['username']
		password = form['password']

		if username == session['user']['_id']:
			# doesn't login just verify the username and password
			error, user = database.login(username, password)
			if user is not None:
				database.get_database().remove_user(username)
				session.clear()
				return redirect(url_for('index'))
		else:
			error = 'You can only delete your account'

		return render_template(
			'delete_account.html',
			user={'is_logged_in': False, 'error': error},
			title='Delete'
		)

	user = {'is_logged_in': True, 'name': session['user']['_id']}
	return render_template(
		'delete_account.html',
		user=user,
		title='Delete'
	)


@app.route('/logout')
def logout():
	"""Logout the user by clearing the session."""
	if 'user' in session:
		session.clear()
	return redirect(url_for('index'))


@app.route('/')
def index():
	"""Render the index page."""
	if 'user' in session:
		user = {'is_logged_in': True, 'name': session['user']['_id']}
	else:
		user = {'is_logged_in': False}
	return render_template(
		'index.html',
		user=user,
		title='HealthIndicator'
	)


@app.route('/data/visualise')
def visualise_data():
	"""
	Visualise the data the end-user has entered
	"""
	if 'user' not in session:
		return redirect(url_for('login'))

	return render_template('visualise_data.html', user={
		'is_logged_in': True,
		'name': session['user']['_id'],
		'data': session['user']['data']
	})
