"""
Test for the Database class
"""
from datetime import datetime

import pytest
import os.path

from health_indicator.health_indicator import HealthIndicator
from health_indicator.user import User


class TestDatabase:
    """
    TestDatabase class in order to test the Database class.
    """

    @pytest.fixture
    def user(self):
        user = User({"username": "JohnDoe", "password": "password", "firstname": "John", "lastname": "Doe",
                     "gender": "M", "birthdate": str(datetime(2000, 1, 1))})
        user["data"].add_data(datetime(2020, 1, 1),
                              {'weight': 70, 'height': 180, 'steps': 10000, 'sleep': 8, 'heartrate': 60})
        return user

    @pytest.fixture
    def database(self):
        database = HealthIndicator(os.path.dirname(os.path.abspath(__file__)) + "/../src/database.key").get_database()
        return database

    def test_add_to_database(self, user, database):
        database.add_user(user)
        assert database.get_user(user["_id"])["firstname"] == user["firstname"]

    def test_update_from_database(self, user, database):
        database.add_user(user)
        user["data"].add_data(datetime(2020, 1, 2), {'weight' : 80})
        database.update_user(user["_id"], user)
        assert database.get_user(user["_id"])["data"]["weight"]["2020-01-02 00:00:00"] == "80"

    def test_encrypt_decrypt(self, user, database):
        encrypted_user = database.encrypt_user(user)
        assert database.decrypt_user(encrypted_user) == user

    def test_remove_from_database(self, user, database):
        database.remove_user(user["_id"])
        assert database.user_exists(user["_id"]) is False
