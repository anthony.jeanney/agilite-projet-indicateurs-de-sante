"""
Test for the User class
"""
from datetime import datetime

from health_indicator.user import User


def test_user():
    user = User({"username": "JohnDoe", "password": "password", "firstname": "John", "lastname": "Doe", "gender": "M", "birthdate": str(datetime(2000, 1, 1))})
    user_data = user["data"]
    assert user_data.get_all_latest_datas() == {"weight": None, "height": None, "steps": None, "sleep": None, "heartrate": None}
    user_data.add_data(datetime(2020, 1, 1), {'weight' : 70, 'height' : 180, 'steps' : 10000, 'sleep' : 8, 'heartrate' : 60})
    assert user_data.get_all_latest_datas() == {"weight": "70", "height": "180", "steps": "10000", "sleep": "8", "heartrate": "60"}
