API documentation
=================

.. autosummary::
    :toctree: _autosummary
    :template: custom-module-template.rst
    :recursive:

    health_indicator.health_indicator
    health_indicator.user
    health_indicator.database
