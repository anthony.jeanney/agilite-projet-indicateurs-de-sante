.. HealthIndicator documentation master file, created by
   sphinx-quickstart on Thu Dec 15 21:56:33 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HealthIndicator's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   description
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
